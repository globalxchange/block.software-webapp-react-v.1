import React, { useContext } from "react";
import { GlobalContext } from "../../context/globalContext";
import hamburger from "../../img/hamburger.png";
import logo from "../../img/logo.png";
import logoWhite from "../../img/logo-white.png";
import { Link, useNavigate } from "react-router-dom";
import { HashLink } from "react-router-hash-link";
import blockLogo from "../../assets/logo.svg";

export const Content = () => {
  const { topNavbar, setTopNavbar } = useContext(GlobalContext);
  const toggleNavbar = () => {
    setTopNavbar(!topNavbar);
  };
  return (
    <div id="hiddenNavbarContent">
      <div
        style={{
          position: "absolute",
          top: "5%",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        onClick={toggleNavbar}
      >
        <Link to="/">
          <img
            src={blockLogo}
            draggable="false"
            alt="logo"
            className="hiddenNavbarLogo"
            id="logoSizeReduce3"
          />
        </Link>
      </div>
      <div id="hiddenNavbarContentlist">
        <HashLink to="/#dev__container" onClick={toggleNavbar}>
          About
        </HashLink>
        <HashLink to="/#services" onClick={toggleNavbar}>
          Web3
        </HashLink>
        <HashLink to="/#services" onClick={toggleNavbar}>
          Tokenization
        </HashLink>
        <HashLink to="/#services" onClick={toggleNavbar}>
          NFT's
        </HashLink>
        <HashLink to="/#services" onClick={toggleNavbar}>
          Defi
        </HashLink>
        <HashLink to="/#services" onClick={toggleNavbar}>
          Wallets
        </HashLink>
        <HashLink to="/#services" onClick={toggleNavbar}>
          Exchanges
        </HashLink>
        <HashLink to="/#services" onClick={toggleNavbar}>
          Whitelabel
        </HashLink>

        {/* <Link to="/portfolio/" onClick={toggleNavbar}>
          OUR PORTFOLIO
        </Link> */}

        {/* <HashLink to="/#process" onClick={toggleNavbar}>
          OUR PROCESS
        </HashLink>

        <HashLink to="/#pricing" onClick={toggleNavbar}>
          PRICING MODEL
        </HashLink>

        <HashLink to="/#contact" onClick={toggleNavbar}>
          DISCUSS PROJECT
        </HashLink> */}
      </div>
      <button onClick={toggleNavbar} className="Header__menu__close"></button>
    </div>
  );
};
